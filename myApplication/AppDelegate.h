//
//  AppDelegate.h
//  myApplication
//
//  Created by appbee technologies on 05/08/16.
//  Copyright © 2016 appbee technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

