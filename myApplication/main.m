//
//  main.m
//  myApplication
//
//  Created by appbee technologies on 05/08/16.
//  Copyright © 2016 appbee technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
